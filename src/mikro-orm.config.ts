import { __prod__ } from "./constants";
import { Post } from "./entities/Post";
import { User } from './entities/User';
import { MikroORM } from "@mikro-orm/core";
import path from 'path';

export default {
  migrations: {
    path: path.join(__dirname, "./migrations"), 
    pattern: /^[\w-]+\d+\.[tj]s$/,
    disableForeignKeys: false,
  },
  entities: [Post, User], // corresponds to all of the db table
  dbName: 'lireddit', // db name
  type: 'postgresql', 
  user: 'redit',
  debug: !__prod__, // check if we are in prod 
} as Parameters<typeof MikroORM.init>[0];